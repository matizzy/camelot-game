from Tkinter import *  #uses tkinter library for gui based gameplay
import tkMessageBox
import threading
import random
import time
import math

pieceMatrix = [[-1,-1,-1,0,0,-1,-1,-1],[-1,-1,0,0,0,0,-1,-1],[-1,0,0,0,0,0,0,-1],[0,0,0,0,0,0,0,0],[0,0,1,1,1,1,0,0],[0,0,0,1,1,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,2,2,0,0,0],[0,0,2,2,2,2,0,0],[0,0,0,0,0,0,0,0],[-1,0,0,0,0,0,0,-1],[-1,-1,0,0,0,0,-1,-1],[-1,-1,-1,0,0,-1,-1,-1]]
#piece matrix will always contain the position of all the pieces.
#-1 is an invalid position, 0 is an empty position, 1 is a white piece position, 2 is a black piece position. 

firstMove = True

human = ''  #will hold whether human player is white or black. 1 is for white, 2 is for black
humanP=6 #number of checkers human player currently has
cpu = '' #will hold whether cpu player is white or black. 1 is for white, 2 is for black
cpuP=6 #number of checkers cpu player has
cpuTurn = '' #keeps track of whether or not it is the cpu's turn to make a move

startGui = ''
boardGui = ''




class move:
	def __init__(self,currentPos,futurePos,curState,player,typeMove):
		self.cpos = currentPos
		self.fpos = futurePos
		self.cstate = curState
		self.eval = None
		#self.fstate = None
		self.player = player
		self.typeMove = typeMove

	def checkNear(self,matrix):
		global human
		global cpu
		dangerCount = 0
		adjCount = 0 
		directions=[[0,0],[0,1],[0,-1],[1,-1][1,0],[1,1],[-1,0],[-1,1],[-1,-1]]
		for row in range(0,14):
			for column in range(0,8):
				if matrix[row][column]==cpu:
					for direction in directions:
						try:
							if matrix[row+direction[0]][column+direction[1]] == human and row+direction[0]>=0 and column+direction[1] >=0:
								dangerCount += 1
						except:
							pass
						try:
							if matrix[row+direction[0]][column+direction[1]] == cpu and row+direction[0]>=0 and column+direction[1] >=0:
								adjCount +=1
						except:
							pass
		return (dangerCount,adjCount)
	
	def distanceScore(self,matrix):
		global human
		global cpu
		score = 0
		for row in range(0,14):
			for column in range(0,8):
				if matrix[row][column]==cpu and cpu == 1:
					score += 14 - math.sqrt(math.pow((13-row),2)+math.pow((3.5- column),2))
				elif matrix[row][column]==cpu and cpu == 2:
					score += math.sqrt(math.pow((row),2)+math.pow((column),2))
				elif matrix[row][column]==human and human == 1:
					score -= 14 - math.sqrt(math.pow((13-row),2)+math.pow((3.5- column),2))
				elif matrix[row][column]==human and human ==2:
					score -= math.sqrt(math.pow((row),2)+math.pow((column),2))
		return score
					
									


	def eval(self):
		
		#global humanP
		#global cpuP
		global human
		global cpu
		matrix,removed = genMatrix(cstate,cpu,oldPos,newPos,typeMove)
		score = 0
		cpieces = 0
		hpieces = 0
		for row in range(0,14):
			for column in range(0,8):
				if matrix[row][column] == 1:
					if human == 1:
						hpieces +=1
					else:
						cpieces +=1
				elif matrix[row][column] ==2:
					if human == 2:
						hpieces +=1
					else:
						cpieces +=1
		if hpieces == 0:
			score += float('inf')
		elif cpieces == 0:
			score -= float('inf')
		elif (cpu == 1 and (matrix[13][3]== 1 or matrix[13][4] == 1)) or (cpu ==2 and (matrix[0][3]==2 or matrix[0][4]==2)):
			score +=float('inf')
		elif (human == 1 and (matrix[13][3]== 1 or matrix[13][4] == 1)) or (human ==2 and (matrix[0][3]==2 or matrix[0][4]==2)):
			score -=float('inf')
		else:
			score += 80*(cpieces-hpieces)
			dangerCount,adjCount = self.checkNear(matrix)
			score -= 40*dangerCount
			score += 30*adjCount
			score += distanceScore(matrix)*12
		return score


class startScreen(Frame):
    def __init__(self,master=None):
        global cpu
        
        Frame.__init__(self, master)
        self.master = master
        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=1)
        self.pack(fill=BOTH, expand=1)
        self.pressed = False
        startB = Button(self, text="Start",command=self.startPress)
        whiteP = Button(self,text="White",command=self.whitePress)
        blackP = Button(self,text="Black",command=self.blackPress)
        whiteP.pack(side=LEFT,padx=5,pady=5)
        blackP.pack(side=LEFT)
        startB.pack(side=LEFT,padx=5,pady=5)
        
    def whitePress(self):
        global human
        global cpu
        human = 1
        cpu = 2
        self.pressed = True
    def blackPress(self):
        global human
        global cpu
        human = 2
        cpu = 1
        self.pressed = True
    def startPress(self):
        global human
        if self.pressed:
            print "The board will now be initialized"
            
            if human%2==0:
                print "The CPU player is White"
                print "The Human player is Black"
            else:
                print "The Human Player is White"
                print "The CPU player is Black"
            self.master.quit()
        else:
            print "You must first select to be either white or black, then press start"
        
        
        

class gameMaster(Frame):
    def __init__(self, master=None):
        global cpuTurn
        Frame.__init__(self, master)
        self.selected=[-1,-1]
        self.pieceSelected = False
        self.count = 0
        self.master = master
        self.canvas = Canvas(master, width=400, height=700)
        self.canvas.bind("<Button-1>", self.onClick)
        self.setupBoard()
        self.drawPieces()
        self.canvas.pack()
        if cpu ==1:
            cpuTurn = True

    def win(self,player):
        tkMessageBox.showinfo("Game Ended!",player + " won!")

    def onClick(self,event):
        global pieceMatrix
        global human
        global cpuTurn
        xcor = int(event.x)
        ycor = int(event.y)
        matPosition=[xcor//50,ycor//50]
        if not cpuTurn:
            if not self.pieceSelected:
                if pieceMatrix[matPosition[1]][matPosition[0]] == human:
                    self.selected = matPosition
                    self.pieceSelected = True
                    self.reDraw()
            elif self.pieceSelected:
                moves,mandatory,cantor = legalMoves(pieceMatrix,human)
                print mandatory
                temp = []
                
                temp.append(str(self.selected[1])+","+str(self.selected[0]))
                temp.append(str(matPosition[1])+","+str(matPosition[0]))
                #print moves
                
                #print temp
                if len(mandatory)>0:
                    if temp in mandatory:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp[0].split(","),temp[1].split(","),2)
                        if removed >0:
                            for f in range(0,removed):
                                removePieceCount(cpu)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win =checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else: 
                            self.win("You")
                    else:
                        self.pieceSelected=False
                        self.selected = [-1,-1]
                        self.reDraw()
                else:
                    if temp in moves:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp[0].split(","),temp[1].split(","),0)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win = checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else: 
                            self.win("You")
                    elif temp in cantor:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp[0].split(","),temp[1].split(","),1)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win = checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else:
                            self.win("You")

                    else:
                        self.pieceSelected=False
                        self.selected = [-1,-1]
                        self.reDraw()

        
        print str(xcor//50) + " " + str(ycor//50)
    def reDraw(self):
        '''this function is for redrawing the gameboard and pieces'''
        if self.count >3:
            self.canvas.delete(ALL)
            self.count = -1
        self.setupBoard()
        self.drawSelectedOutline(self.selected)
        self.drawPieces()
        self.canvas.pack()
        #self.selected = [-1,-1]
        self.count +=1

    def drawSelectedOutline(self,coordinates):
        '''this function is for outlining a selected box or piece with a blue square
        It takes a matrix position as its input'''
        self.canvas.create_rectangle(coordinates[0]*50,coordinates[1]*50,(coordinates[0]+1)*50,(coordinates[1]+1)*50,outline="blue",width=4)
    def setupBoard(self):
        global pieceMatrix
        for row in range(0,14):
            for column in range(0,8):
                if (pieceMatrix[row][column]==0 or pieceMatrix[row][column]==1 or pieceMatrix[row][column]==2):
                    if ((column+row) %2 == 0):
                        self.canvas.create_rectangle(column*50,row*50,(column+1)*50,(row+1)*50,fill = "green")
                    else:
                        self.canvas.create_rectangle(column*50,row*50,(column+1)*50,(row+1)*50,fill = "yellow")
    def drawPieces(self):
        '''draws pieces on the gameboard. Uses the global pieceMatrix'''
        global pieceMatrix
        for row in range(0,14):
            for column in range(0,8):
                if (pieceMatrix[row][column] == 1):
                    self.canvas.create_oval(column*50,row*50,(column+1)*50,(row+1)*50,fill="white")
                elif(pieceMatrix[row][column]==2):
                    self.canvas.create_oval(column*50,row*50,(column+1)*50,(row+1)*50,fill="black")
        

def legalMoves(matrix,color):
    '''returns a list of lists. Each list inside the big list is a legal move. 
    must pass in the current piece matrix and the color for which all legals moves must be calculated
    color must be 1 or 2. 1 is for white, 2 is for black'''
    moves = []
    mandatoryMoves=[]
    cantorMoves = []
    ncolor = ''
    if color ==1:
        ncolor = 2
    else:
        ncolor = 1
    directions = [[1,0,1,0,2,0],[-1,0,-1,0,-2,0],[0,1,0,1,0,2],[0,-1,0,-1,0,-2],[1,1,1,1,2,2],[1,-1,1,-1,2,-2],[-1,1,-1,1,-2,2],[-1,-1,-1,-1,-2,-2]]
    for row in range(0,14):
        for column in range(0,8):
            if pieceMatrix[row][column] == color:
                #print "got match"
                for direction in directions:
                    try:
                        if pieceMatrix[row+direction[0]][column+direction[1]]==0:
                            #print "found legal move"
                            if not((row+direction[0])<0 or (column+direction[1])<0):
                                tempList = []
                                tempList.append(str(row)+","+str(column))
                                tempList.append(str(row+direction[0])+","+str(column+direction[1]))
                            #print tempList
                                moves.append(tempList)
                        elif (pieceMatrix[row+direction[2]][column+direction[3]]==color and pieceMatrix[row+direction[4]][column+direction[5]] == 0):
                            if not((row+direction[2])<0 or (column+direction[3]) <0 or (row+direction[4])<0 or (column+direction[5])<0):
                                tempList = []
                                tempList.append(str(row)+","+str(column))
                                tempList.append(str(row+direction[4])+","+str(column+direction[5]))
                                cantorMoves.append(tempList)
                        elif (pieceMatrix[row+direction[2]][column+direction[3]]==ncolor and pieceMatrix[row+direction[4]][column+direction[5]] == 0):
                            if not((row+direction[2])<0 or (column+direction[3]) <0 or (row+direction[4])<0 or (column+direction[5])<0):
                                tempList = []
                                tempList.append(str(row)+","+str(column))
                                tempList.append(str(row+direction[4])+","+str(column+direction[5]))
                                mandatoryMoves.append(tempList)
                    except:
                        pass
                
    return moves,mandatoryMoves,cantorMoves

def genMatrix(matrix,color,oldPos,newPos,moveType):
    '''generates a new piece matrix. takes matrix and color as parameters. 
    also needs the old position, the newPosition, and the move type. 
    0 is for regular move, 1 is for cantor, and 2 is for attack

    To do: implement a function that can expand on cantor and attack for the extra credit'''
    global human
    global cpu
    global cpuP
    global humanP
    ncolor = ''
    if color ==1:
        ncolor = 2
    else:
        ncolor = 1
    calledByHuman = ''
    if ncolor == cpu:
        calledByHuman = True
    else:
        calledByHuman = False
    tempMatrix = matrix
    tempMatrix[int(oldPos[0])][int(oldPos[1])] = 0
    tempMatrix[int(newPos[0])][int(newPos[1])] = color
    removed = 0
    if moveType == 2:
        rd = 0
        cd = 0
        tempr = int(oldPos[0])
        tempc = int(oldPos[1])
        rcorrect = ''
        ccorrect = ''
        if int(newPos[1])>int(oldPos[1]):
            cd = 1
        elif int(newPos[1])<int(oldPos[1]):
            cd = -1
        if int(newPos[0])>int(oldPos[0]):
            rd =1
        elif int(newPos[0])<int(oldPos[0]):
            rd = -1
        if rd != 0:
            rcorrect = False
        else:
            rcorrect = True
        if cd != 0:
            ccorrect = False
        else:
            ccorrect = True
        while not(rcorrect and ccorrect):
            tempMatrix[tempr+rd][tempc+cd]=0
            removed +=1
            tempr = tempr+ 2*rd
            tempc = tempc + 2*cd
            if tempr == int(newPos[0]):
                rd = 0
                rcorrect = True
            if tempc == int(newPos[1]):
                cd = 0
                ccorrect = True


    return (tempMatrix,removed)

def dumbAI():
    global pieceMatrix
    global boardGui
    global cpu
    global cpuTurn
    global firstMove
    while True:
        if (cpuTurn):
            moveType = ''
            if firstMove:
                firstMove = False
                time.sleep(1)
            print "cpu Turn started"
            moves,mandatory,cantor =  legalMoves(pieceMatrix,cpu)
            #print moves
            chosenMove = ''
            if len(mandatory)>0:
                chosenMove = mandatory[random.randint(0,len(mandatory)-1)]
                moveType = 2
            else:
                r = random.randint(0,1)
                if r == 0:
                    chosenMove = moves[random.randint(0,len(moves)-1)]
                    moveType = 0
                else:
                    if len(cantor) >0:
                        chosenMove = cantor[random.randint(0,len(cantor)-1)]
                        moveType = 1
                    else:
                        chosenMove = moves[random.randint(0,len(moves)-1)]
                        moveType = 0

            
            oldPos = chosenMove[0].split(",")
            newPos = chosenMove[1].split(",")
            pieceMatrix,removed = genMatrix(pieceMatrix,cpu,oldPos,newPos,moveType)
            if removed >0:
                for i in range(0,removed):
                    removePieceCount(human)
            win = checkWin(pieceMatrix,cpu)
            boardGui.reDraw()
            if not win:
                cpuTurn = False
            else:
                boardGui.win("Computer")

def removePieceCount(colorOfPlayerToRemove):
    global human
    global humanP
    global cpuP
    if colorOfPlayerToRemove == human:
        humanP = humanP -1
    else:
        cpuP = cpuP-1
def checkOpponentPieceCount(yourOwnColor):
    enemyColor =''
    global human
    global humanP
    global cpuP
    if yourOwnColor ==1:
        enemyColor = 2
    else:
        enemyColor =1
    if enemyColor == human:
        return humanP
    else:
        return cpuP


def checkWin(matrix,color):
    win = False
    if color==2:
        if matrix[0][3] == color or matrix[0][4]==color:
            win = True
    elif color == 1:
        if matrix[13][3] == color or matrix[13][4]==color:
            win = True
    if checkOpponentPieceCount(color) ==0:
        win = True
    return win



def guiThread():
    global startGui
    global boardGui
    root=Tk()
    startGui = startScreen(master=root)
    startGui.mainloop()
    root.destroy()
    root = Tk()
    boardGui = gameMaster(master=root)
    boardGui.mainloop()
    root.destroy()    
    
gui = threading.Thread(target = guiThread)
gui.start()
dai = threading.Thread(target=dumbAI)
dai.start()
while True:
    pass