def alphaBeta(matrix, depth =0, maxmin =True, alpha = -float('inf'),beta = float('inf')):
    '''the parameters that must be passed in:
    matrix(list of lists): the matrix positions at the node in the depth currently being checked
    depth(int): the current depth that is being checked. Default depth is 0
    maxmin(bool): True for max, False for maxmin. Default value is True because depth 0 is a max player
    alpha(float): the current alpha value. Default value is negative infinity
    beta(float):the current beta value. Default value is positive infinity. 
    '''
    global maxDepth
    #if depth == maxDepth - 1:
    moves,mtype =  consolidateMoves(legalMoves(matrix,cpu)):
    tempAl = alpha
    tempBe = beta
    
    #below is the block of code for when the depth is 1 less than max depth
    if depth == maxDepth -1:
        if maxmin:
            #val = tempAl
            index = 0
            if len(moves) == 0:
                return tempAl,tempBe
            else:
                #bestMove = None
                flag = True
                while (index <len(moves) and flag):
                    score = moves[index].eval()
                    if score > tempBe:
                        flag = False
                        tempAl = alpha
                    elif (score >tempAl and score<=tempBe):
                        tempAl = score
        else:
            val = tempBe
            index = 0
            if len(moves) == 0:
                return tempAl,tempBe
            else:
                flag = True
                while(index <len(moves) and flag):
                    score = moves[index].eval()
                    if score < tempAl:
                        flag = False
                        tempBe = beta
                    elif (score <tempBe and score>=tempAl):
                        tempBe = score
        return tempAl, tempBe

    #below is the code for depths 1 to maxDepth -2
    elif depth >0 and depth < (maxDepth -1):
        if maxmin:
            for move in moves:
                tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)
                al,be = alphaBeta(tempMat,depth+1,False,tempAl,tempBe)
                if be>tempAl:
                    tempAl = be
            return tempAl,tempBe
        else:
            for move in moves:
                tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)
                al,be = alphaBeta(tempMat,depth+1,True,tempAl,tempBe)
                if al<tempBe:
                    tempBe = al
            return tempAl,tempBe

    #below is the code for the depth 0

elif depth == 0:
    bestIndex = 0
    bestVal = -float("inf")
    curIndex = 0
    for move in moves:
        tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)
        al,be = alphaBeta(tempMat,depth+1,False,tempAl,tempBe)
        if al>bestVal:
            bestVal = al
            bestIndex = curIndex
            curIndex +=1
        else:
            curIndex +=1
    moves[bestIndex].executeMove()







    
     
