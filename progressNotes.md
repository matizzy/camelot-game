Simplified Camelot Game
=======================
#####Python (2.7) Implementation using Tkinter

######By Matthew Izberskiy - mi737  (matizzy)


Task         | Progress     |date
---------------- | --------------------
Create gameboard      | done | 04/04/15
Create pieces             | done | 04/04/15
Create initilal screen             | done | 04/05/15
Create white/black select | done  | 04/05/15
Write AI | to be done  | TBA
Put game together | to be done | TBA
