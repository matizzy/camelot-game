from Tkinter import *  #uses tkinter library for gui based gameplay
import tkMessageBox
import threading
import random
import time
import math
import copy

pieceMatrix = [[-1,-1,-1,0,0,-1,-1,-1],[-1,-1,0,0,0,0,-1,-1],[-1,0,0,0,0,0,0,-1],[0,0,0,0,0,0,0,0],[0,0,1,1,1,1,0,0],[0,0,0,1,1,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,2,2,0,0,0],[0,0,2,2,2,2,0,0],[0,0,0,0,0,0,0,0],[-1,0,0,0,0,0,0,-1],[-1,-1,0,0,0,0,-1,-1],[-1,-1,-1,0,0,-1,-1,-1]]
#piece matrix will always contain the position of all the pieces.
#-1 is an invalid position, 0 is an empty position, 1 is a white piece position, 2 is a black piece position.

firstMove = True

human = ''  #will hold whether human player is white or black. 1 is for white, 2 is for black
humanP=6 #number of checkers human player currently has
cpu = '' #will hold whether cpu player is white or black. 1 is for white, 2 is for black
cpuP=6 #number of checkers cpu player has
cpuTurn = '' #keeps track of whether or not it is the cpu's turn to make a move

startGui = ''    #Depreciated
boardGui = ''    #Depreciated

maxDepth = 6  #Set by the user selecting

TIME = 0         #TIME and TIMESTART are global variables used to hold time values. TIME is always incremented
TIMESTART = 0    #TIMESTART is set at the beginning of every CPU turn to the current time
 
BESTMOVES = []   #Hold best moves attained by the alphaBeta algorithm, the very best move is at the end of the list

'''The variables below are used to keep track of how many cuts and the depth reached. It is used for printing and these variables
are reset at the beginning of every CPU turn'''
CURMAXCUTS = 0    
TOTMAXCUTS = 0

CURMINCUTS = 0
TOTMINCUTS = 0

CURNODES = 0
TOTNODES = 0

REACHEDDEPTH = 2

def timer():
    '''this function is a simple timer. It increments TIME by 1 every second'''
    global TIME
    while True:
        time.sleep(1)
        TIME +=1




class move:

    def __init__(self,currentPos,futurePos,curState,player,typeMove):
        '''move class. Hass to be initialized with the current position, the future position, the current game state(a matrix),
        the player, either CPU or Human, and the type of move (0,1,2)'''
        self.cpos = currentPos
        self.fpos = futurePos
        self.cstate = copy.deepcopy(curState)
        

        #self.fstate = None
        self.player = player
        self.typeMove = typeMove

    def checkNear(self,matrix):
        '''function that returns how many adjacenies there are after this move would be performed and how many pieces are in danger'''
        global human
        global cpu
        dangerCount = 0
        adjCount = 0
        directions=[[0,0],[0,1],[0,-1],[1,-1],[1,0],[1,1],[-1,0],[-1,1],[-1,-1]]
        for row in range(0,14):
            for column in range(0,8):
                if matrix[row][column]==cpu:
                    for direction in directions:
                        try:
                            if matrix[row+direction[0]][column+direction[1]] == human and row+direction[0]>=0 and column+direction[1] >=0:
                                dangerCount += 1
                        except:
                            pass
                        try:
                            if matrix[row+direction[0]][column+direction[1]] == cpu and row+direction[0]>=0 and column+direction[1] >=0:
                                adjCount +=1
                        except:
                            pass
        return (dangerCount,adjCount)

    def distanceScore(self,matrix):
        '''calculates the distance from the goal'''
        global human
        global cpu
        score = 0
        for row in range(0,14):
            for column in range(0,8):
                if matrix[row][column]==cpu and cpu == 1:
                    score += 14 - math.sqrt(math.pow((13-row),2)+math.pow((3.5- column),2))
                elif matrix[row][column]==cpu and cpu == 2:
                    score += math.sqrt(math.pow((row),2)+math.pow((column),2))
                #elif matrix[row][column]==human and human == 1:
                    #score -= 14 - math.sqrt(math.pow((13-row),2)+math.pow((3.5- column),2))
                #elif matrix[row][column]==human and human ==2:
                    #score -= math.sqrt(math.pow((row),2)+math.pow((column),2))
        return score




    def eval(self):
        '''returns a value based on how good the move is'''

        #global humanP
        #global cpuP
        global human
        global cpu
        #print "before eval\n"
        #print pieceMatrix
        matrix,removed = genMatrix(copy.deepcopy(self.cstate),cpu,self.cpos[0].split(","),self.fpos[0].split(","),self.typeMove)
        #print "after eval"
       # print pieceMatrix
        score = 0
        cpieces = 0
        hpieces = 0
        for row in range(0,14):
            for column in range(0,8):
                if matrix[row][column] == 1:
                    if human == 1:
                        hpieces +=1
                    else:
                        cpieces +=1
                elif matrix[row][column] ==2:
                    if human == 2:
                        hpieces +=1
                    else:
                        cpieces +=1
        if hpieces == 0:
            score += 9999999999
        elif cpieces == 0:
            score -= 9999999999
        elif (cpu == 1 and (matrix[13][3]== 1 or matrix[13][4] == 1)) or (cpu ==2 and (matrix[0][3]==2 or matrix[0][4]==2)):
            score +=9999999999
        elif (human == 1 and (matrix[13][3]== 1 or matrix[13][4] == 1)) or (human ==2 and (matrix[0][3]==2 or matrix[0][4]==2)):
            score -=9999999999
        else:
            score += 2000*(cpieces-hpieces)
            dangerCount,adjCount = self.checkNear(matrix)
            score -= 1000*dangerCount
            score += 1000*adjCount
            score += self.distanceScore(matrix)*50000

        return score

    def __eq__(self,other):
        '''function that is used to check if one move is equal to another. This is used for checking if the human move is a valid move'''
        if self.cpos == other.cpos and self.fpos == other.fpos and self.cstate == other.cstate:
            return True
        else:
            return False

    def __ne__(self,other):
        '''opposite of __eq__'''
        return not self.__eq__(other)

    def __getitem__(self,key):
        '''depreciated, not used'''
        if key == 0:
            print self.cpos
            print type(self.cpos)
            print type(self.cpos[0])
            return self.cpos[0]
        elif key==0:
            #print self.fpos
            return self.fpos[0]

    def executeMove(self):
        '''executes the move by updating the global piece matrix'''
        global cpu
        global pieceMatrix
        global human
        #print "move executed"
        #print "matrix before evaluation\n"
        #print pieceMatrix
        matrix,removed = genMatrix(copy.deepcopy(self.cstate),cpu,self.cpos[0].split(","),self.fpos[0].split(","),self.typeMove)
        pieceMatrix = matrix
        #print "resulatant Matrix\n"
        #print pieceMatrix
        if removed >0:
            for i in range(0,removed):
                if self.player == human:
                    removePieceCount(cpu)
                else:
                    removePieceCount(human)



class startScreen(Frame):
    '''class for the gui for selecting the if the human wants to be white or black'''
    def __init__(self,master=None):
        global cpu

        Frame.__init__(self, master)
        self.master = master
        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=1)
        self.pack(fill=BOTH, expand=1)
        self.pressed = False
        startB = Button(self, text="Start",command=self.startPress)
        whiteP = Button(self,text="White",command=self.whitePress)
        blackP = Button(self,text="Black",command=self.blackPress)
        whiteP.pack(side=LEFT,padx=5,pady=5)
        blackP.pack(side=LEFT)
        startB.pack(side=LEFT,padx=5,pady=5)

    def whitePress(self):
        global human
        global cpu
        human = 1
        cpu = 2
        self.pressed = True
    def blackPress(self):
        global human
        global cpu
        human = 2
        cpu = 1
        self.pressed = True
    def startPress(self):
        global human
        if self.pressed:
            print "The board will now be initialized"

            if human%2==0:
                print "The CPU player is White"
                print "The Human player is Black"
            else:
                print "The Human Player is White"
                print "The CPU player is Black"
            self.master.quit()
        else:
            print "You must first select to be either white or black, then press start"


class difficultyScreen(Frame):
    '''class for the gui for selecting the difficulty'''
    def __init__(self,master=None):
        global maxDepth

        Frame.__init__(self, master)
        self.master = master
        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=1)
        self.pack(fill=BOTH, expand=1)
        self.pressed = False
        easyP = Button(self, text="easy",command=self.easyPress)
        mediumP = Button(self,text="medium",command=self.mediumPress)
        hardP = Button(self,text="hard",command=self.hardPress)
        startP = Button(self,text="start",command=self.startPress)
        easyP.pack(side=LEFT,padx=5,pady=5)
        mediumP.pack(side=LEFT)
        hardP.pack(side=LEFT)
        startP.pack(side=LEFT,padx=5,pady=5)

    def easyPress(self):
        global maxDepth
        maxDepth = 2
        self.pressed = True
    def mediumPress(self):
        global maxDepth
        maxDepth = 4
        self.pressed = True
    def hardPress(self):
        global maxDepth
        maxDepth = 6
        self.pressed=True

    def startPress(self):
        global human
        if self.pressed:
            self.master.quit()
        else:
            print "You must first select a difficulty and then press start"




class gameMaster(Frame):
    '''class for the gameboard gui. Also handles human moves'''
    def __init__(self, master=None):
        global cpuTurn
        Frame.__init__(self, master)
        self.selected=[-1,-1]
        self.pieceSelected = False
        self.count = 0
        self.master = master
        self.canvas = Canvas(master, width=400, height=700)
        self.canvas.bind("<Button-1>", self.onClick)
        self.setupBoard()
        self.drawPieces()
        self.canvas.pack()
        if cpu ==1:
            cpuTurn = True

    def win(self,player):
        '''displays a "win" message'''
        tkMessageBox.showinfo("Game Ended!",player + " won!")

    def onClick(self,event):
        '''this function is what handles all of the humans's moves.

        it works by checking to see if a human clicks a valid piece and then presses a valid position. If so, it performs that move'''
        global pieceMatrix
        global human
        global cpuTurn
        xcor = int(event.x)
        ycor = int(event.y)
        matPosition=[xcor//50,ycor//50]
        if not cpuTurn:
            if not self.pieceSelected:
                if pieceMatrix[matPosition[1]][matPosition[0]] == human:
                    self.selected = matPosition
                    self.pieceSelected = True
                    self.reDraw()
            elif self.pieceSelected:
                moves,mandatory,cantor = legalMoves(pieceMatrix,human)
                #print mandatory
                temp = move([str(self.selected[1])+","+str(self.selected[0])],[str(matPosition[1])+","+str(matPosition[0])],pieceMatrix,human,0)
                #print type(temp)

                #print moves

                #print temp
                if len(mandatory)>0:
                    if temp in mandatory:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp.cpos[0].split(","),temp.fpos[0].split(","),2)
                        if removed >0:
                            for f in range(0,removed):
                                removePieceCount(cpu)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win =checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else:
                            self.win("You")
                    else:
                        self.pieceSelected=False
                        self.selected = [-1,-1]
                        self.reDraw()
                else:
                    if temp in moves:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp.cpos[0].split(","),temp.fpos[0].split(","),0)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win = checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else:
                            self.win("You")
                    elif temp in cantor:
                        pieceMatrix,removed=genMatrix(pieceMatrix,human,temp.cpos[0].split(","),temp.fpos[0].split(","),1)
                        self.selected = [-1,-1]
                        self.reDraw()
                        self.pieceSelected=False
                        win = checkWin(pieceMatrix,human)
                        if not win:
                            cpuTurn = True
                        else:
                            self.win("You")

                    else:
                        self.pieceSelected=False
                        self.selected = [-1,-1]
                        self.reDraw()


        print str(xcor//50) + " " + str(ycor//50)
    def reDraw(self):
        '''this function is for redrawing the gameboard and pieces'''
        if self.count >3:
            self.canvas.delete(ALL)
            self.count = -1
        self.setupBoard()
        self.drawSelectedOutline(self.selected)
        self.drawPieces()
        self.canvas.pack()
        #self.selected = [-1,-1]
        self.count +=1

    def drawSelectedOutline(self,coordinates):
        '''this function is for outlining a selected box or piece with a blue square
        It takes a matrix position as its input'''
        self.canvas.create_rectangle(coordinates[0]*50,coordinates[1]*50,(coordinates[0]+1)*50,(coordinates[1]+1)*50,outline="blue",width=4)
    def setupBoard(self):
        global pieceMatrix
        for row in range(0,14):
            for column in range(0,8):
                if (pieceMatrix[row][column]==0 or pieceMatrix[row][column]==1 or pieceMatrix[row][column]==2):
                    if ((column+row) %2 == 0):
                        self.canvas.create_rectangle(column*50,row*50,(column+1)*50,(row+1)*50,fill = "green")
                    else:
                        self.canvas.create_rectangle(column*50,row*50,(column+1)*50,(row+1)*50,fill = "yellow")
    def drawPieces(self):
        '''draws pieces on the gameboard. Uses the global pieceMatrix'''
        global pieceMatrix
        for row in range(0,14):
            for column in range(0,8):
                if (pieceMatrix[row][column] == 1):
                    self.canvas.create_oval(column*50,row*50,(column+1)*50,(row+1)*50,fill="white")
                elif(pieceMatrix[row][column]==2):
                    self.canvas.create_oval(column*50,row*50,(column+1)*50,(row+1)*50,fill="black")


def legalMoves(matrix,color):
    '''returns a list of lists. Each list inside the big list is a legal move.
    must pass in the current piece matrix and the color for which all legals moves must be calculated
    color must be 1 or 2. 1 is for white, 2 is for black'''
    moves = []
    mandatoryMoves=[]
    cantorMoves = []
    ncolor = ''
    if color ==1:
        ncolor = 2
    else:
        ncolor = 1
    flag = True
    directions = [[1,0,1,0,2,0],[-1,0,-1,0,-2,0],[0,1,0,1,0,2],[0,-1,0,-1,0,-2],[1,1,1,1,2,2],[1,-1,1,-1,2,-2],[-1,1,-1,1,-2,2],[-1,-1,-1,-1,-2,-2]]
    #print type(matrix)
    for row in range(0,14):
        for column in range(0,8):
            if matrix[row][column] == color:
                #print "got match"
                for direction in directions:
                    try:
                        if matrix[row+direction[0]][column+direction[1]]==0:
                            #print "found legal move"
                            if not((row+direction[0])<0 or (column+direction[1])<0) and flag:
                                tempMove = move([str(row)+","+str(column)],[str(row+direction[0])+","+str(column+direction[1])],matrix,color,0)

                            #print tempList
                                moves.append(tempMove)
                        elif (matrix[row+direction[2]][column+direction[3]]==color and matrix[row+direction[4]][column+direction[5]] == 0):
                            if not((row+direction[2])<0 or (column+direction[3]) <0 or (row+direction[4])<0 or (column+direction[5])<0) and flag:
                                tempMove = move([str(row)+","+str(column)],[str(row+direction[4])+","+str(column+direction[5])],matrix,color,1)
                                cantorMoves.append(tempMove)
                        elif (matrix[row+direction[2]][column+direction[3]]==ncolor and matrix[row+direction[4]][column+direction[5]] == 0):
                            if not((row+direction[2])<0 or (column+direction[3]) <0 or (row+direction[4])<0 or (column+direction[5])<0):
                                flag = False
                                moves = []
                                cantorMoves = []
                                tempList = []
                                tempMove = move([str(row)+","+str(column)],[str(row+direction[4])+","+str(column+direction[5])],matrix,color,2)
                                mandatoryMoves.append(tempMove)
                    except:
                        pass

    return moves,mandatoryMoves,cantorMoves

def genMatrix(matrix,color,oldPos,newPos,moveType):
    '''generates a new piece matrix. takes matrix and color as parameters.
    also needs the old position, the newPosition, and the move type.
    0 is for regular move, 1 is for cantor, and 2 is for attack

    To do: implement a function that can expand on cantor and attack for the extra credit'''
    global human
    global cpu
    global cpuP
    global humanP
    ncolor = ''
    if color ==1:
        ncolor = 2
    else:
        ncolor = 1
    calledByHuman = ''
    if ncolor == cpu:
        calledByHuman = True
    else:
        calledByHuman = False
    tempMatrix = None
    if type(matrix)==type((1,2,3)):
        tempMatrix = matrix[0]
    else:
        tempMatrix = matrix
    #print tempMatrix
    tempMatrix[int(oldPos[0])][int(oldPos[1])] = 0
    tempMatrix[int(newPos[0])][int(newPos[1])] = color
    removed = 0
    if moveType == 2:
        rd = 0
        cd = 0
        tempr = int(oldPos[0])
        tempc = int(oldPos[1])
        rcorrect = ''
        ccorrect = ''
        if int(newPos[1])>int(oldPos[1]):
            cd = 1
        elif int(newPos[1])<int(oldPos[1]):
            cd = -1
        if int(newPos[0])>int(oldPos[0]):
            rd =1
        elif int(newPos[0])<int(oldPos[0]):
            rd = -1
        if rd != 0:
            rcorrect = False
        else:
            rcorrect = True
        if cd != 0:
            ccorrect = False
        else:
            ccorrect = True
        while not(rcorrect and ccorrect):
            tempMatrix[tempr+rd][tempc+cd]=0
            removed +=1
            tempr = tempr+ 2*rd
            tempc = tempc + 2*cd
            if tempr == int(newPos[0]):
                rd = 0
                rcorrect = True
            if tempc == int(newPos[1]):
                cd = 0
                ccorrect = True


    return (tempMatrix,removed)

'''def basicAI():
    global pieceMatrix
    global boardGui
    global cpu
    global cpuTurn
    global firstMove
    while True:
        time.sleep(0.1)
        if (cpuTurn):
            
            moveType = ''
            if firstMove:
                firstMove = False
                time.sleep(1)
            print "cpu Turn started"
            basicMoves,mandatoryMoves,candorMoves =  legalMoves(pieceMatrix,cpu)
            

            print "#basic moves: "+ str(len(basicMoves))
            print "#mandatoryMoves: " + str(len(mandatoryMoves))
            print "#cantorMoves: " + str(len(candorMoves))
            #print moves
            index = 0
            score = None
            if len(mandatoryMoves)>0:
        
                for i in range(len(mandatoryMoves)):
                    tempScore = mandatoryMoves[i].eval()
                    if tempScore>score:
                        score = tempScore
                        index = i
                print "performing mandatory move"
                mandatoryMoves[index].executeMove()
                
            
            else:
                for move in candorMoves:
                    basicMoves.append(move)
                candorMoves=[]
                print "numMoves total: " + str(len(basicMoves))
                for i in range(len(basicMoves)):
                    #print "numMoves total: " + str(len(basicMoves))
                    #print basicMoves[i].fpos
                    #print type(basicMoves[i])
                    
                    tempScore = basicMoves[i].eval()
                    
                    #print pieceMatrix
                    if tempScore > score:
                        score = tempScore
                        index=i

                #print pieceMatrix
                basicMoves[index].executeMove()

            win = checkWin(pieceMatrix,cpu)
            boardGui.reDraw()
            
            if not win:
                cpuTurn = False
            else:
                boardGui.win("Computer")'''

def resetGlobals():
    '''this function resets the globals that are used by the AI'''
    global BESTMOVES
    global CURNODES
    global TOTNODES
    global CURMAXCUTS
    global TOTMAXCUTS
    global CURMINCUTS
    global TOTMINCUTS
    global REACHEDDEPTH
    BESTMOVES = []
    CURNODES = 0
    TOTNODES = 0
    CURMAXCUTS = 0
    TOTMAXCUTS = 0
    CURMINCUTS = 0
    TOTMINCUTS = 0
    REACHEDDEPTH = 0


def basicAI():
    '''this function is the controller for the alpha beta algorithm'''
    global pieceMatrix
    global boardGui
    global cpuTurn
    global cpu
    global TIME
    global TIMESTART
    global maxDepth
    global BESTMOVES
    global CURNODES
    global TOTNODES
    global CURMAXCUTS
    global TOTMAXCUTS
    global CURMINCUTS
    global TOTMINCUTS
    global REACHEDDEPTH
    curMax = maxDepth
    reached = 2
    while True:
        time.sleep(0.1)  #sleeps so that the processor is not overtaxed
        if (cpuTurn):  #checks to see if it's the computer's turn
            resetGlobals()
            reached = 2
            TIMESTART = TIME
            for i in range(2,maxDepth+1):  #for loops that runs the alphaBeta algorithm for different max depths. This ensures that if the maxDepth is not reached, the next best depth move is performed
                maxDepth = i
                
                #inside a try/except block because when the timer runs out, it can cause algorithm to crash.

                try:
                    d,maxc,minc,nodes=alphaBeta(copy.deepcopy(pieceMatrix))
                except:
                    pass

                print "done depth: " + str(d)
                if d> reached:
                    reached = d
                if maxc>TOTMAXCUTS:
                    TOTMAXCUTS = maxc
                if minc>TOTMINCUTS:
                    TOTMINCUTS = minc
                if CURNODES > TOTNODES:
                    TOTNODES = CURNODES
            #maxDepth = curMax  #resets maxDepth to original value
            


            #alphaBeta(copy.deepcopy(pieceMatrix))
            BESTMOVES[len(BESTMOVES)-1].executeMove()  #performs the best move
            boardGui.reDraw()
            print "Total Nodes explored: " +str(TOTNODES)
            print "Total Depth Reached: " +str(reached)
            print "total Max cuts: " + str(TOTMAXCUTS)
            print "total min cuts" + str(TOTMINCUTS)
            win = checkWin(pieceMatrix,cpu)
            if not win:           #win checking
                cpuTurn = False
            else:
                boardGui.win("Computer")

def removePieceCount(colorOfPlayerToRemove):
    '''updates the piece count'''
    global human
    global humanP
    global cpuP
    if colorOfPlayerToRemove == human:
        humanP = humanP -1
    else:
        cpuP = cpuP-1
def checkOpponentPieceCount(yourOwnColor):
    '''checks how much pieces the human has'''
    enemyColor =''
    global human
    global humanP
    global cpuP
    if yourOwnColor ==1:
        enemyColor = 2
    else:
        enemyColor =1
    if enemyColor == human:
        return humanP
    else:
        return cpuP


def consolidateMoves(basic,man,can):
    '''consolidtes moves into one list for easier traversing'''
    if len(man)>0:
        return (man,2)
    else:
        for move in can:
            basic.append(move)
        can=[]
    return (basic,0)

def alphaBeta(matrix, depth =0, maxmin =True, alpha = -float('inf'),beta = float('inf')):
    '''the parameters that must be passed in:
    matrix(list of lists): the matrix positions at the node in the depth currently being checked
    depth(int): the current depth that is being checked. Default depth is 0
    maxmin(bool): True for max, False for maxmin. Default value is True because depth 0 is a max player
    alpha(float): the current alpha value. Default value is negative infinity
    beta(float):the current beta value. Default value is positive infinity. 
    '''
    global maxDepth
    global BESTMOVES
    global TIME
    global TIMESTART
    global CURMINCUTS
    global CURMAXCUTS
    global CURNODES
    #if depth == maxDepth - 1:
    #print "alpha beta called"
    
    if (TIME - TIMESTART < 10):
        #print TIME - TIMESTART

        b,m,c = legalMoves(matrix,cpu)
        moves,mtype =  consolidateMoves(b,m,c)
        tempAl = alpha
        tempBe = beta
        
        #below is the block of code for when the depth is 1 less than max depth
        if depth == maxDepth -1:
            #print "at depth: " + str(depth)
            if maxmin:
                #print "at max player"
                #val = tempAl
                index = 0
                if len(moves) == 0:
                    return tempAl,tempBe
                else:
                    #bestMove = None
                    flag = True
                    while (index <len(moves) and flag):
                        score = moves[index].eval()
                        if score > tempBe:
                            flag = False
                            tempAl = alpha
                            CURNODES +=1
                            CURMAXCUTS +=1
                        elif (score >tempAl and score<=tempBe):
                            CURNODES +=1
                            tempAl = score
                        else:
                            CURNODES +=1
                        index += 1
            else:
                #print "at min player"
                val = tempBe
                index = 0
                if len(moves) == 0:
                    return tempAl,tempBe
                else:
                    flag = True
                    while(index <len(moves) and flag):
                        score = moves[index].eval()
                        if score < tempAl:
                            flag = False
                            tempBe = beta
                            CURMINCUTS +=1
                            CURNODES+=1
                        elif (score <tempBe and score>=tempAl):
                            CURNODES+=1
                            tempBe = score
                        else:
                            CURNODES+=1
                        index +=1
            return tempAl, tempBe

        #below is the code for depths 1 to maxDepth -2
        elif depth >0 and depth < (maxDepth -1):
            if maxmin:
                for move in moves:
                    tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)[0]
                    al,be = alphaBeta(tempMat,depth+1,False,tempAl,tempBe)
                    if be>tempAl:
                        tempAl = be
                return tempAl,tempBe
            else:
                for move in moves:
                    tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)[0]
                    al,be = alphaBeta(tempMat,depth+1,True,tempAl,tempBe)
                    if al<tempBe:
                        tempBe = al
                return tempAl,tempBe

        #below is the code for the depth 0

        #if (TIMESTART - TIME<10):
        elif depth == 0:
            bestIndex = 0
            bestVal = -float("inf")
            curIndex = 0
            #print "about to go through moves"
            for move in moves:
                #print curIndex
                tempMat = genMatrix(copy.deepcopy(matrix),cpu,move.cpos[0].split(","),move.fpos[0].split(","),mtype)[0]
                #print "about to make recursive call"
                al,be = alphaBeta(tempMat,depth+1,False,tempAl,tempBe)
                #print al
                if be>bestVal:
                    bestVal = be
                    bestIndex = curIndex
                    curIndex +=1
                else:
                    curIndex +=1
            print bestIndex
            if TIMESTART-TIME<10:
                BESTMOVES.append(copy.deepcopy(moves[bestIndex]))
                return maxDepth,CURMAXCUTS,CURMINCUTS,CURNODES
    else:
        print "over time"




def checkWin(matrix,color):
    '''win condition checking'''
    win = False
    if color==2:
        if matrix[0][3] == color or matrix[0][4]==color:
            win = True
    elif color == 1:
        if matrix[13][3] == color or matrix[13][4]==color:
            win = True
    if checkOpponentPieceCount(color) ==0:
        win = True
    return win



def guiThread():
    '''the gui thread'''
    global startGui
    global boardGui
    root=Tk()
    startGui = startScreen(master=root)
    startGui.mainloop()
    root.destroy()
    root = Tk()
    difficultGUI = difficultyScreen(master=root)
    difficultGUI.mainloop()
    root.destroy()
    root = Tk()
    boardGui = gameMaster(master=root)
    boardGui.mainloop()
    root.destroy()
#below is the code for starting the threads and running them
timeThread = threading.Thread(target = timer)
timeThread.start()
gui = threading.Thread(target = guiThread)
gui.start()
dai = threading.Thread(target=basicAI)
dai.start()
#the while loop below keeps this main thread alive. Could be depreciated in future version
while True:
    time.sleep(1)
    pass
